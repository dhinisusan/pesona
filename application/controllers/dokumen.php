<?php
Class Dokumen extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('m_dokumen');
        $this->load->helper('url');

    }
    
    public function index(){
    	$data["pageTitle"]      = "Dokumen";
        $data['title']          = 'Dokumen';
        $data['main_content']   = 'dokumen/dokumen';     
        $config['base_url']     = "http://localhost/pesona/index.php/dokumen";    
        $data['dokumen']        = $this->m_dokumen->get_all();
    
        	$this->template->load('template','dokumen',$data);
    }

    public function add_dokumen()
    {
  		$data["pageTitle"] = "Dokumen";
        $data['status'] = $this->m_dokumen->getStatus();
        $data['bagian'] = $this->m_dokumen->getBagian();
        $data['kategori'] = $this->m_dokumen->getKategori();

      		$this->template->load('template','f_adddokumen',$data);
    }

    function tambah_dokumen(){
        $data['title'] = 'Tambah Dokumen';
        $data['main_content'] = 'f_adddokumen';


        $this->load->library('upload'); //panggil libary upload

            $extension = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);

            $namafile                =$this->input->post('filename'); //nama file + fungsi time
            $config['upload_path']   = FCPATH.'/uploads/'; //Folder untuk menyimpan hasil upload
            $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf|doc|xls'; //type yang dapat diakses bisa anda sesuaikan
            $config['file_name']     = $namafile; //nama yang terupload nantinya

            $this->upload->initialize($config); //initialisasi upload dari array config
            $file_image_poto = $this->upload->data();

            $this->upload->do_upload('upload');

        //depan (database) belakang(html)
                $data=[
                    
                    'filename'=>$namafile,
                    'tipe_file'=>$extension,
                    'size'=>$this->input->post('status'),
                    'tgl_upload'=>$this->input->post('tgl_upload'),
                    'jenis_bagian'=>$this->input->post('jns_bagian'),
                    'kategori'=>$this->input->post('jns_kategori'),
                    'keterangan'=>$this->input->post('keterangan'),

                    
                ];
                
                $this->m_dokumen->insert($data);
                $this->session->set_flashdata('notif', "Data berhasil ditambahkan");
                redirect('index.php/dokumen');
            
        
    }

     public function edit($id_file)
    {
        $data["pageTitle"] = "Edit DOkumen";
        $data['row'] = $this->m_dokumen->get_data_by_id($id_file);
        
        $this->template->load('template','dokumen/f_editdokumen',$data);
    }

     public function update_dokumen()
    {
          $data=[
                    
                    'filename'=>$namafile,
                    'tipe_file'=>$extension,
                    'size'=>$this->input->post('status'),
                    'tgl_upload'=>$this->input->post('tgl_upload'),
                    'jenis_bagian'=>$this->input->post('jns_bagian'),
                    'kategori'=>$this->input->post('jns_kategori'),
                    'keterangan'=>$this->input->post('keterangan'),

                    
                ];
                
                $this->m_dokumen->update($this->input->post('id_file'),$data);
                $this->session->set_flashdata('notif', "Data berhasil ditambahkan");
                redirect('index.php/dokumen');
    }

    public function delete($id_file)
    {
        $this->m_dokumen->delete($id_file);
        redirect('index.php/dokumen');
    }


    public function getNamaDoc(){
        $id_file = $this->input->post('id_file');
        $res = $this->m_dokumen->getDokumen(array('
        id_file'=>$id_file));

        $hasil = "Nama Dokumen";
        if(count($res)>0){
            $hasil = $res[0]['filename'];
        }
        echo $hasil;
    }  
    
}