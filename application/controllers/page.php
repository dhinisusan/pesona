<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Page extends CI_Controller{
    
    function __construct(){  
	  parent::__construct();  
    }

    public function index(){
        $data['title'] = "Beranda";
        $this->template->load('template','index');
         
    }
}