<?php
Class Kategori extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('m_kategori');
        $this->load->helper('url');
    }
    
    function index(){

        $data["pageTitle"] = "Kategori";
        $data['main_content'] = 'kategori/kategori';
        
        $config['base_url'] = "http://localhost/pesona/index.php/kategori";
        
        $data['kategori'] = $this->m_kategori->get_all();
        $this->template->load('template','kategori',$data);
    }

    function add_kategori(){
        $data["pageTitle"] = "Tambah kategori";
        $this->template->load('template','f_addkategori');
    }

    function tambah_kategori(){
        $data['title'] = 'Tambah kategori';
        $data['main_content'] = 'f_addkategori';


        
                $data=[
                    
                    'kategoriname'=>$this->input->post('nama_kt'),
                    'keterangan'=>$this->input->post('ket_kt'),
                    
                ];
                $this->m_kategori->insert($data);
                $this->session->set_flashdata('notif', "Data berhasil ditambahkan");
                redirect('index.php/kategori');
            
        
    }

    public function delete($id_kt)
    {
        $this->m_kategori->delete($id_kt);
        redirect('index.php/kategori');
    }

    public function edit($id_kt)
    {
        $data["pageTitle"] = "Edit kategori";
        $data['row'] = $this->m_kategori->get_data_by_id($id_kt);
        
        $this->template->load('template','kategori/f_editkategori',$data);
    }

    public function view($id_kt)
    {
        $data["pageTitle"] = "View kategori";
        $data['row'] = $this->m_kategori->get_data_by_id($id_kt);
        
        $this->template->load('template','kategori/f_viewkategori',$data);
    }

    public function update_kategori()
    {
         $data=[
                    
                    'kategoriname'=>$this->input->post('nama_kt'),
                    'keterangan'=>$this->input->post('ket_kt'),
                    
                ];
        $this->m_kategori->update($this->input->post('id_kategori'),$data);
        redirect('index.php/kategori');
    }
}