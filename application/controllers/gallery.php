<?php
Class Gallery extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('m_gallery');
        $this->load->helper('url');
    }
    
    function index(){
    $data["pageTitle"] = "Gallery";
        $data['main_content'] = 'gallery/gallery';
        
        $config['base_url'] = "http://localhost/pesona/index.php/gallery";
        
       // $data['gallery'] = $this->m_gallery->tampil_gallery()->result();
    $this->template->load('template','gallery',$data);
    }
}