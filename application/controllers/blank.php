<?php
Class Blank extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('m_blank');
        $this->db->join('tkategori','id_kategori=kategori');
        $this->db->join('tbagian','id_bagian=jenis_bagian');
        $this->load->helper('url');
    }
    
    public function index(){
    	$data["pageTitle"] = "Testing";

        $data['title'] = 'Testing';
        $data['main_content'] = 'blank/blank'; 
        $config['base_url'] = "http://localhost/pesona/index.php/blank";
        

        $data['dokumen'] = $this->m_blank->tampil_data()->result();
    	$this->template->load('template','blank',$data);
    }
}
