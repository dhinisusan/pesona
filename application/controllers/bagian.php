<?php
Class Bagian extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('m_bagian');
        $this->load->helper('url');
    }
    
    function index(){

    	$data["pageTitle"] = "Bagian";
        $data['main_content'] = 'bagian/bagian';
        
        $config['base_url'] = "http://localhost/pesona/index.php/bagian";
        
        $data['bagian'] = $this->m_bagian->get_all();
        $this->template->load('template','bagian',$data);
    }

    function add_bagian(){
    	$data["pageTitle"] = "Tambah bagian";
        $this->template->load('template','f_addbagian');
    }

    function tambah_bagian(){
        $data['title'] = 'Tambah Bagian';
        $data['main_content'] = 'f_addbagian';


        
                $data=[
                    
                    'bagianname'=>$this->input->post('nama_bg'),
                    'keterangan'=>$this->input->post('ket_bg'),
                    
                ];
                $this->m_bagian->insert($data);
                $this->session->set_flashdata('notif', "Data berhasil ditambahkan");
                redirect('index.php/bagian');
            
        
    }

    public function delete($id_bg)
    {
        $this->m_bagian->delete($id_bg);
        redirect('index.php/bagian');
    }

    public function edit($id_bg)
    {
        $data["pageTitle"] = "Edit bagian";
        $data['row'] = $this->m_bagian->get_data_by_id($id_bg);
        
        $this->template->load('template','bagian/f_editbagian',$data);
    }

    public function view($id_bg)
    {
        $data["pageTitle"] = "View bagian";
        $data['row'] = $this->m_bagian->get_data_by_id($id_bg);
        
        $this->template->load('template','bagian/f_viewbagian',$data);
    }

    public function update_bagian()
    {
         $data=[
                    
                    'bagianname'=>$this->input->post('nama_bg'),
                    'keterangan'=>$this->input->post('ket_bg'),
                    
                ];
        $this->m_bagian->update($this->input->post('id_bagian'),$data);
        redirect('index.php/bagian');
    }
}