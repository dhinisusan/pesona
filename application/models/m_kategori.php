<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class M_Kategori extends CI_Model{

    public $table = 'tkategori';
    public $id    = 'id_kategori';
    public $order = 'DESC';

  public function __CONSTRUCT(){
    parent::__CONSTRUCT();
  }

  //get all
  public function get_all()
  {
    $this->db->order_by('id_kategori','ASC');
    return $this->db->get($this->table)->result();
  }

  //get data by id
  public function get_data_by_id($id)
  {
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  //insert data
  public function insert($data)
  {
    // print_r($data);
    // exit();
    $this->db->insert($this->table, $data);
  }

  //update data
  public function update($id,$data)
  {
   // print_r($data);
   // print_r($id);
   //  exit(); 
    $this->db->where($this->id, $id);
    $this->db->update($this->table, $data);
  }

//delete data
  public function delete($id,$data)
  {
    $this->db->where($this->id, $id);
    $this->db->delete($this->table);
  }
}