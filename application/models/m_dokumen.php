<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class M_dokumen extends CI_Model{
	
    public $table = 'tfile';
    public $id    = 'id_file';
    public $order = 'DESC';

  public function __CONSTRUCT(){
    parent::__CONSTRUCT();
  }

  //get all
  public function get_all()
  {
    $this->db->join('tkategori','id_kategori=kategori');
    $this->db->join('tbagian','id_bagian=jenis_bagian');
    $this->db->join('tstatus','id_status=size','left outer');
    $this->db->order_by('id_file','ASC');
    return $this->db->get($this->table)->result();
  }

  //get data by id
  public function get_data_by_id($id)
  {
    $this->db->where($this->id,$id);
    return $this->db->get($this->table)->row();
  }

  //insert data
  public function insert($data)
  {
    // print_r($data);
    // exit();
    $this->db->insert($this->table, $data);
  }

  //update data
  public function update($id,$data)
  {
   // print_r($data);
   // print_r($id);
   //  exit(); 
    $this->db->where($this->id, $id);
    $this->db->update($this->table, $data);
  }

//delete data
  public function delete($id,$data)
  {
    $this->db->where($this->id, $id);
    $this->db->delete($this->table);
  }

//fungsi untuk dropdown select
  function getKategori($where = array()){
    $query = $this->db->get('tkategori');
    return $query->result();
  }

  function getStatus(){
    $query = $this->db->get('tstatus');
    return $query->result();
  }

  function getBagian(){
    $query = $this->db->get('tbagian');
    return $query->result();
  }

}